# -> str : se llama type hinting. Le estás diciendo al linter (corrector de sintaxis) que la función siempre devuelve un string.
# Por ejemplo, si en otra parte del código escribís generar_nombre() + 1, el editor de texto te lo subraya en rojo porque no podés sumar strings y números.

# https://carbon.now.sh/tFFEk7kjE3TkxBWZo5iD
# Códigos Python: Generador de nombres fantasy aleatorios
# Desarrollado por Leandro Barone
# Complejidad del código: ★☆☆☆☆
import random

# Iterables (cadenas y tuplas) con piezas para generar nombres
VOCALES = 'aeiou'
CONSONANTES = 'bcdfghjklmnprstvwxyz'
SUFIJOS = 'ckson', 'ngton', 'gwyn', 'ndolin', 'sson', 'rindae'
ELEMENTOS = 'blood', 'fire', 'flame', 'thunder', 'shadow', 'poison'
SUSTANTIVOS = 'sword', 'axe', 'spear', 'hammer', 'wing', 'cloak'
ADJETIVOS = 'noble', 'brave', 'strong', 'nimble', 'wise', 'bright'

def generar_nombre() -> str:
  	# Creamos un alias de choice() sin "ensuciar" el contexto global
	r = random.choice
    # Generamos las partes del nombre
	nombre = r(CONSONANTES) + r(VOCALES) + r(SUFIJOS)
	apellido = r(ELEMENTOS) + r(SUSTANTIVOS)
	adjetivo = r(ADJETIVOS)
	return f'{nombre} {apellido} the {adjetivo}'.title()

for _ in range(10): print(generar_nombre())
# => Vugwyn Bloodsword The Wise, Tosson Thundercloak The Brave, ...
